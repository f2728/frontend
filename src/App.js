import * as React from "react";
import Nav from "./components/Nav";
import Map from "./components/Map";
import "./App.css";


function App() {

  return (
    <div>
      <Map />
      <Nav />
    </div>
  );
}

export default App;
