import React, {useEffect, useState} from "react";
import { MapContainer, TileLayer,Marker,Popup } from "react-leaflet";
import "./style.css";
import axios from "axios";

const Map = () => {
    const [coordinateX, setCoordinateX] = useState(48); //48.2038024
    const [coordinateY, setCoordinateY] = useState(16); //16.3922587
    const [data, setData] = useState([]);

    useEffect(() => {
        navigator.geolocation.getCurrentPosition(function (position) {
            setCoordinateX(position.coords.latitude);
            setCoordinateY(position.coords.longitude);
        });
    }, [coordinateX, coordinateY]);


    useEffect(async()=>
    {
        const response = await axios.get(`https://fys.orange-pangolin.com/backend/api/locations?x=${coordinateY}&y=${coordinateX}`)
        setData(response.data);
        console.log(data);
    },[coordinateX, coordinateY]);
    console.log(coordinateX);
    console.log(coordinateY);
  return (
    <div className="map">
      <MapContainer
        center={[coordinateX, coordinateY]}
        zoom={10}
        scrollWheelZoom={true}
      >
        <TileLayer
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        />
          {data.map((item, index) =>{
              return (
                  <div key={index}>
                      <Marker position={[item.fountainCoordinate.y,item.fountainCoordinate.x]}>
                          <Popup>
                              Trees
                              <h1>{item.treeCount}</h1>
                          </Popup>
                      </Marker>
                  </div>
              )
          })}
          <Marker position={[coordinateX, coordinateY]}>
              <Popup>
                  Your current position!
              </Popup>
          </Marker>
      </MapContainer>
    </div>
  );
};

export default Map;
