import React, { useState, useEffect } from "react";
import BottomNavigation from "@mui/material/BottomNavigation";
import BottomNavigationAction from "@mui/material/BottomNavigationAction";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import "./style.css";

function Nav() {
  const [coordinateX, setCoordinateX] = useState("48.2038024");
  const [coordinateY, setCoordinateY] = useState("16.3922587");
  const [value, setValue] = React.useState(0);
  useEffect(() => {
    navigator.geolocation.getCurrentPosition(function (position) {
      setCoordinateX(position.coords.latitude);
      setCoordinateY(position.coords.longitude);
    });
  }, [coordinateX, coordinateY]);

  const locationHandel = () => {
    localStorage.setItem("coordinateX", coordinateX);
    localStorage.setItem("coordinateY", coordinateY);
  };
  return (
    <div>
      <BottomNavigation
        className="nav"
        showLabels
        value={value}
        onChange={(event, newValue) => {
          setValue(newValue);
        }}
      >
        <BottomNavigationAction
          onClick={locationHandel}
          label="your location"
          icon={<LocationOnIcon />}
        />
      </BottomNavigation>
    </div>
  );
}

export default Nav;
